
# Start kafka
In summary, for Linux (ex: Ubuntu)
Download and Setup Java 8 JDK:

sudo apt install openjdk-8-jdk
Download & Extract the Kafka binaries from https://kafka.apache.org/downloads

Try Kafka commands using bin/kafka-topics.sh (for example)

Edit PATH to include Kafka (in ~/.bashrc for example) PATH="$PATH:/your/path/to/your/kafka/bin"

Edit Zookeeper & Kafka configs using a text editor

zookeeper.properties: dataDir=/your/path/to/data/zookeeper

server.properties: log.dirs=/your/path/to/data/kafka

Start Zookeeper in one terminal window: zookeeper-server-start.sh config/zookeeper.properties

Start Kafka in another terminal window: kafka-server-start.sh config/server.properties

# deal with topics

## create a topic
kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --topic new_topic --partitions 3 --replication-factor 1

## list topics
kafka-topics.sh --zookeeper 127.0.0.1:2181 --list

## describe the topic
kafka-topics.sh --zookeeper 127.0.0.1:2181 --describe --topic new_topic

## delete the topic
kafka-topics.sh --zookeeper 127.0.0.1:2181 --delete --topic new_topic

## listen topic
kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic new_topic

## groups : describe
kafka-consumer-groups --bootstrap-server 127.0.0.1:9092 --group kafka-demo-elasticsearch --describe

## group : reset offset
kafka-consumer-groups --bootstrap-server 127.0.0.1:9092 --group kafka-demo-elasticsearch --reset-offsets --topic twitter_tweets --execute --to-earliest


# Configuring Producers and Consumers
Client Configurations
There exist a lot of options to:

configure producer: https://kafka.apache.org/documentation/#producerconfigs

configure consumers:  https://kafka.apache.org/documentation/#consumerconfigs

The most important options are discussed in the real-world project section, coming next

# Elasticsearch

## Check the elasticsearch works properly
https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-health.html

### list health info
curl -X GET http://localhost:9200/_cat/health\?v

### list nodes info
curl -X GET http://localhost:9200/_cat/nodes\?v

### list all indices
curl -X GET http://localhost:9200/_cat/indices\?v

## Create index
curl -X PUT http://localhost:9200/twitter

## Put data to index
curl -H "Content-Type: application/json" --data "{\"test\":\"test\"}" http://localhost:9200/twitter/test/1

## Get data from index
curl -X GET http://localhost:9200/twitter/test/1

## Delete message
curl -X DELETE http://localhost:9200/twitter/test/1


